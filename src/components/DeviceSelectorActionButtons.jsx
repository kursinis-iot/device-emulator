import {AddIcon, DownloadIcon} from "@chakra-ui/icons";
import {IconButton} from "@chakra-ui/react";
import {UploadIcon} from "./icons/UploadIcon.jsx";
import {useState} from "react";
import {DeviceConfig} from "./DeviceConfig.jsx";
import fileDownload from "js-file-download";


export function DeviceSelectorActionButtons({openConfig, exportActive, exportConfig, openImport}) {
    return <>
        <IconButton
            position={"fixed"}
            bottom={"20px"}
            right={"20px"}
            borderRadius={100}
            size={"lg"}
            colorScheme='green'
            aria-label='Create fake device'
            icon={<AddIcon/>}
            onClick={openConfig}
        />

        <IconButton
            position={"fixed"}
            bottom={"88px"}
            right={"20px"}
            borderRadius={100}
            size={"lg"}
            colorScheme='green'
            aria-label='Export config'
            isDisabled={!exportActive}
            icon={<DownloadIcon/>}
            onClick={exportConfig}
        />

        <IconButton
            position={"fixed"}
            bottom={"156px"}
            right={"20px"}
            borderRadius={100}
            size={"lg"}
            colorScheme='green'
            aria-label='Import config'
            icon={<UploadIcon/>}
            onClick={openImport}
        />
    </>;
}