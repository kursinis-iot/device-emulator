import {useState} from "react";
import {Device} from "./Device.jsx";
import {Tab, TabList, TabPanel, TabPanels, Tabs} from "@chakra-ui/react";
import {DeviceSelectorActions} from "./DeviceSelectorActions.jsx";

export function DeviceSelector() {
    const [devicesConfig, setDevicesConfig] = useState([]);

    function removeDevice(index) {
        const newConfig = [...devicesConfig];
        newConfig.splice(index, 1)
        setDevicesConfig(newConfig);
    }

    return <Tabs>
        <TabList>
            {devicesConfig.map((config, index) => <Tab key={index}>{index}</Tab>)}
        </TabList>
        <TabPanels>
            {devicesConfig.map((config, index) =>
                <TabPanel key={index}>
                    <Device config={config} removeDevice={() => removeDevice(index)}/>
                </TabPanel>
            )}
        </TabPanels>
        <DeviceSelectorActions devicesConfig={devicesConfig} setDevicesConfig={setDevicesConfig}/>
    </Tabs>
}