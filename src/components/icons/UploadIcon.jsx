import {DownloadIcon} from "@chakra-ui/icons";

export function UploadIcon(props) {
    return <DownloadIcon {...props} transform={"rotate(180deg)"}/>
}