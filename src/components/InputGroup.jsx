import {FormControl, FormErrorMessage, FormLabel, Input} from "@chakra-ui/react";

export function InputGroup({id, label, type = "text", placeholder = '', value, onChange, errors = [], ...props}) {
    return <FormControl py={4} {...props} isInvalid={errors.length !== 0}>
        <FormLabel htmlFor={id}>
            {label}
        </FormLabel>
        <Input
            id={id}
            type={type}
            placeholder={placeholder}
            value={value}
            onChange={(e) => onChange(e.target.value)}
        />
        {errors.map((error, index) => <FormErrorMessage key={index}>{error}</FormErrorMessage>)}
    </FormControl>
}