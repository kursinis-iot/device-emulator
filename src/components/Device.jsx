import {useEffect, useState} from "react";
import {
    Accordion,
    AccordionButton, AccordionIcon,
    AccordionItem, AccordionPanel,
    Box, Button,
    Container,
    Heading, Slider, SliderFilledTrack, SliderMark, SliderThumb, SliderTrack,
    Tab,
    TabList,
    TabPanel, TabPanels,
    Tabs,
    Text
} from "@chakra-ui/react";
import jp from "jsonpath";

export function Device({config, removeDevice}) {
    const {platform, device} = config;
    const {sensors, configs} = device;

    const [sensorValues, setSensorValues] = useState(sensors.map((sensor) => sensor.default));
    const [configValues, setConfigValues] = useState(configs.map(config => undefined))
    const [websocket, setWebsocket] = useState(null);
    const [connectionAttempt, setConnectionAttempt] = useState(1);

    const setSensorValue = function (index, value) {
        const sensorValuesCopy = [...sensorValues];
        sensorValuesCopy[index] = value;
        setSensorValues(sensorValuesCopy);
    }

    useEffect(() => {
        let closed = false;

        console.log("Opening Connection to " + platform.url);
        const webSocket = new WebSocket(`${platform.url}?uuid=${device.uuid}&apiKey=${device.apiKey}`);

        webSocket.addEventListener("open", function () {
            console.log("Connected to " + platform.url);
            setWebsocket(webSocket);
        });

        webSocket.addEventListener('message', function ({data: _data}) {
            const data = JSON.parse(_data);

            if (data.type === "ok")
                return;

            setConfigValues(configs.map((config) => jp.query(data.config, config.query)));
        });

        webSocket.addEventListener('close', function ({reason, code}) {
            console.log(`WS closed by remote code: ${code}, reason: ${reason}!`);
            webSocket.close();
            setWebsocket(null);
            closed = true;
        });

        return () => {
            if (closed)
                return;

            console.log("WS closed by us!");
            setWebsocket(null);
            webSocket.close();
            closed = true;
        }
    }, [connectionAttempt]);

    useEffect(
        () => {
            if (websocket === null)
                return;

            const intervalId = setInterval(() => {
                const data = {
                    sensors: {}
                };

                sensors.forEach((sensor, index) => {
                    data.sensors[sensor.key] = sensorValues[index];
                });

                websocket.send(JSON.stringify(data));
            }, device.updateInterval);

            return () => {
                clearInterval(intervalId);
            }
        }, [websocket, sensorValues]
    )

    const sliderLabelStyles = {
        mt: '2',
        ml: '-2.5',
        fontSize: 'sm',
    }

    return <Container textAlign={"center"} mt={10}>
        <Heading mb={5}>Device Emulator</Heading>
        <Text>Device Uuid: {device.uuid}.</Text>
        <Text>Device Api Key: {device.apiKey}.</Text>
        <Text>Web socket status: {websocket !== null ? "Connected" : "Disconnected"}.</Text>
        <Text>Connection attempt: #{connectionAttempt}</Text>
        <Text>Update interval: {device.updateInterval} ms.</Text>

        <Box my={5} display="flex" justifyContent="center">
            {websocket === null ? <Button
                size="sm"
                mx={1}
                colorScheme="yellow"
                onClick={() => setConnectionAttempt(connectionAttempt + 1)}
            >Reconnect</Button> : null}
            <Button
                size="sm"
                mx={1}
                colorScheme="red"
                onClick={removeDevice}
            >Remove</Button>
        </Box>

        <Tabs isFitted>
            <TabList>
                <Tab>Sensors</Tab>
                <Tab>Configuration</Tab>
                <Tab>Web socket logs</Tab>
            </TabList>

            <TabPanels>
                <TabPanel p={0}>
                    <Accordion>
                        {sensors.map((sensor, index) => <AccordionItem key={index}>
                            <AccordionButton>
                                <Box as="span" flex='1' textAlign='left'>
                                    Sensor #{index}: {sensor.key}, value: {sensorValues[index]}
                                </Box>
                                <AccordionIcon/>
                            </AccordionButton>

                            <AccordionPanel>

                                <Slider
                                    aria-label={sensors[index].key}
                                    value={sensorValues[index]}
                                    min={sensors[index].min}
                                    max={sensors[index].max}
                                    onChange={setSensorValue.bind(this, index)}
                                >
                                    <SliderMark value={sensors[index].min} {...sliderLabelStyles}>
                                        {sensors[index].min}
                                    </SliderMark>
                                    <SliderMark value={sensors[index].max} {...sliderLabelStyles}>
                                        {sensors[index].max}
                                    </SliderMark>
                                    <SliderTrack>
                                        <SliderFilledTrack/>
                                    </SliderTrack>
                                    <SliderThumb/>
                                </Slider>

                            </AccordionPanel>
                        </AccordionItem>)}
                    </Accordion>
                </TabPanel>

                <TabPanel>
                    {configs.map((config, index) => <Box display="flex" justifyContent="space-between" key={index}>
                        <Text>Config: {config.title}</Text>
                        <Text>Value: {JSON.stringify(configValues[index])}</Text>
                    </Box>)}
                </TabPanel>

                <TabPanel p={0}>

                </TabPanel>
            </TabPanels>
        </Tabs>
    </Container>;
}