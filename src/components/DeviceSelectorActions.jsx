import {useState} from "react";
import {DeviceConfig} from "./DeviceConfig.jsx";
import fileDownload from "js-file-download";
import {DeviceSelectorActionButtons} from "./DeviceSelectorActionButtons.jsx";
import {DeviceConfigImport} from "./DeviceConfigImport.jsx";


export function DeviceSelectorActions({devicesConfig, setDevicesConfig}) {
    const [action, setAction] = useState(undefined);

    function clearAction() {
        setAction(undefined);
    }

    function addConfig(config) {
        setDevicesConfig([...devicesConfig, config]);
        clearAction();
    }

    function exportConfig() {
        fileDownload(
            JSON.stringify(devicesConfig),
            "config.json"
        );
    }

    function importConfig(config) {
        setDevicesConfig(config);
        clearAction();
    }

    switch (action) {
        case "config":
            return <DeviceConfig setConfig={addConfig} cancel={clearAction}/>
        case 'import':
            return <DeviceConfigImport cancel={clearAction} submit={importConfig}/>
        default:
            return <DeviceSelectorActionButtons
                exportConfig={exportConfig}
                exportActive={devicesConfig.length !== 0}
                openConfig={() => setAction('config')}
                openImport={() => setAction('import')}
            />
    }
}