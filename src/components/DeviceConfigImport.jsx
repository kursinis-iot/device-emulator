import {
    Button,
    FormControl,
    FormLabel,
    Input,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay
} from "@chakra-ui/react";

export function DeviceConfigImport({cancel, submit}) {
    function onInputChange(e) {
        const file = e.target.files[0];

        const reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = (e) => submit(JSON.parse(e.target.result));
    }

    return <Modal isOpen={true} onClose={cancel}>
        <ModalOverlay/>
        <ModalContent>
            <ModalHeader>Import Emulator State</ModalHeader>
            <ModalCloseButton/>
            <ModalBody>
                <FormControl py={4}>
                    <FormLabel htmlFor={"fileImport"}>
                        State file
                    </FormLabel>
                    <Input
                        pt={1}
                        id={"fileImport"}
                        type={"file"}
                        placeholder={"Choose file"}
                        onChange={onInputChange}
                    />
                </FormControl>
            </ModalBody>

            <ModalFooter display="flex" justifyContent="center">
                <Button
                    colorScheme={"yellow"}
                    onClick={cancel}
                    mt={5}
                >Cancel</Button>
            </ModalFooter>
        </ModalContent>
    </Modal>
}