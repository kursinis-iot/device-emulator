import {
    Accordion,
    AccordionButton,
    AccordionIcon,
    AccordionItem,
    AccordionPanel,
    Box,
    Button,
    Card,
    CardBody, Center,
    Container,
    Heading,
    Tab,
    TabList,
    TabPanel,
    TabPanels,
    Tabs
} from "@chakra-ui/react";
import {InputGroup} from "./InputGroup.jsx";
import {useState} from "react";
import {DeleteIcon} from "@chakra-ui/icons";

const defaultUrl = import.meta.env.VITE_DEVICE_CONNECTOR_URL;

export function DeviceConfig({setConfig, cancel}) {
    const [deviceUuid, setDeviceUuid] = useState('');
    const [deviceApiKey, setDeviceApiKey] = useState('');
    const [url, setUrl] = useState(defaultUrl);
    const [updateInterval, setUpdateInterval] = useState(1000);

    const [sensors, setSensors] = useState([{
        key: "temperature",
        min: -100,
        max: 200,
        default: 20
    }]);

    const [configs, setConfigs] = useState([{
        title: "Lempa 1",
        query: '$.lamps[0]'
    }]);

    const [errors, setErrors] = useState(null);

    const addConfig = () => {
        setConfigs([
            ...configs,
            {
                title: "",
                query: "$."
            }
        ]);
    };

    const deleteConfig = (index) => {
        const configsCopy = [...configs];
        configsCopy.splice(index, 1);
        setConfigs(configsCopy);
    }

    const setConfigValue = (index, key, value) => {
        const configsCopy = [...configs];
        configsCopy[index][key] = value;
        setConfigs(configsCopy);
    }

    const addSensor = () => {
        setSensors([
            ...sensors,
            {
                uuid: "sensor",
                min: 0,
                max: 100,
                default: 50
            }
        ])
    }

    const deleteSensor = (index) => {
        const sensorsCopy = [...sensors];
        sensorsCopy.splice(index, 1);
        setSensors(sensorsCopy);
    }

    const setSensorConfigValue = (index, key, value) => {
        const parsed = parseInt(value);
        if (!isNaN(parsed))
            value = parsed;

        const sensorsCopy = [...sensors];
        sensorsCopy[index][key] = value;
        setSensors(sensorsCopy);
    };

    const validateSensors = function () {
        const errors = [];

        sensors.forEach(
            (sensor, index) => {
                const sensorErrors = {};

                if (sensor.key.length === 0)
                    sensorErrors.key = ["This field is required!"];
                else if (sensor.key.length >= 64)
                    sensorErrors.key = ["This field is limited to 64 characters!"];

                if (sensor.min >= sensor.max) {
                    sensorErrors.min = ['Min should be less than or equal to max!'];
                    sensorErrors.max = ['Max should be more than or equal to min!'];
                }

                if (sensor.default > sensor.max)
                    sensorErrors.default = ['Default should be less than or equal to max!'];
                else if (sensor.default < sensor.min)
                    sensorErrors.default = ['Default should be more than or equal to min!'];

                if (Object.keys(sensorErrors).length !== 0)
                    errors[index] = sensorErrors;
            }
        )

        return errors;
    }

    const validate = function () {
        const errors = {};

        if (deviceUuid.length === 0)
            errors.deviceUuid = ["This field is required!"];

        else if (deviceUuid.length > 64)
            errors.deviceUuid = ["This field is limited to 64 characters!"];

        if (deviceApiKey.length === 0)
            errors.deviceApiKey = ["This field is required!"];

        else if (deviceApiKey.length > 64)
            errors.deviceApiKey = ["This field is limited to 64 characters!"];

        if (url.length === 0)
            errors.url = ["This field is required!"];

        if (updateInterval <= 0)
            errors.updateInterval = ["Update interval should be greater than 0!"];

        const sensorErrors = validateSensors();
        if (sensorErrors.length !== 0)
            errors.sensors = sensorErrors;

        if (Object.keys(errors).length === 0)
            return true;

        setErrors(errors);
        return false;
    }

    const submit = function () {
        setErrors(null);
        if (!validate())
            return;

        setConfig({
            device: {
                uuid: deviceUuid,
                apiKey: deviceApiKey,
                updateInterval: updateInterval,
                sensors: sensors,
                configs: configs
            },
            platform: {
                url: url
            }
        });
    }

    return <Center
        position={"fixed"}
        top={0}
        left={0}
        width={"100vw"}
        height={"100vh"}
        backgroundColor={"#00000070"}
    >
        <Container width={"xl"}>
            <Card>
                <CardBody py={10}>
                    <Heading textAlign={"center"} mb={10}>Configure the device!</Heading>

                    <Tabs isFitted>
                        <TabList>
                            <Tab>Main</Tab>
                            <Tab>Sensors</Tab>
                            <Tab>Configuration</Tab>
                        </TabList>

                        <TabPanels>
                            <TabPanel p={0}>
                                <InputGroup
                                    id="config_device_uuid"
                                    label="Device uuid:"
                                    type="text"
                                    value={deviceUuid}
                                    onChange={setDeviceUuid}
                                    errors={errors?.deviceUuid}
                                />

                                <InputGroup
                                    id="config_device_api_key"
                                    label="Device api key:"
                                    type="text"
                                    value={deviceApiKey}
                                    onChange={setDeviceApiKey}
                                    errors={errors?.deviceApiKey}
                                />

                                <InputGroup
                                    id="config_device_update_interval"
                                    label="Device's update interval (ms):"
                                    type="number"
                                    value={updateInterval}
                                    onChange={setUpdateInterval}
                                    errors={errors?.updateInterval}
                                />

                                <InputGroup
                                    id="config_platform_url"
                                    label="Platform URL:"
                                    type="text"
                                    value={url}
                                    onChange={setUrl}
                                    errors={errors?.url}
                                />
                            </TabPanel>
                            <TabPanel p={0}>
                                <Accordion allowMultiple>
                                    {sensors.map((sensor, index) => <AccordionItem key={index}>
                                        <AccordionButton>
                                            <Box as="span" flex='1' textAlign='left'
                                                 color={errors?.sensors !== undefined && errors.sensors[index] !== undefined ? "red" : null}>
                                                Sensor #{index}: {sensor.key}
                                            </Box>
                                            <AccordionIcon/>
                                        </AccordionButton>

                                        <AccordionPanel px={0}>
                                            <Button
                                                colorScheme={"red"}
                                                ml={2}
                                                aria-label={"Delete"}
                                                isDisabled={sensors.length === 1}
                                                onClick={deleteSensor.bind(this, index)}
                                                display={"block"}
                                                mx={"auto"}
                                            >
                                                Delete <DeleteIcon ml={2}/>
                                            </Button>

                                            <InputGroup
                                                id={`config_sensor_${index}_key`}
                                                label={`Sensor key:`}
                                                type="text"
                                                value={sensor.key}
                                                onChange={setSensorConfigValue.bind(this, index, 'key')}
                                                errors={errors?.sensors !== undefined ? errors.sensors[index]?.key : undefined}
                                            />
                                            <InputGroup
                                                id={`config_sensor_${index}_min`}
                                                label={`Sensor min value:`}
                                                type="number"
                                                value={sensor.min}
                                                onChange={setSensorConfigValue.bind(this, index, 'min')}
                                                errors={errors?.sensors !== undefined ? errors.sensors[index]?.min : undefined}
                                            />
                                            <InputGroup
                                                id={`config_sensor_${index}_max`}
                                                label={`Sensor max value:`}
                                                type="number"
                                                value={sensor.max}
                                                onChange={setSensorConfigValue.bind(this, index, 'max')}
                                                errors={errors?.sensors !== undefined ? errors.sensors[index]?.max : undefined}
                                            />
                                            <InputGroup
                                                id={`config_sensor_${index}_default`}
                                                label={`Sensor default value:`}
                                                type="number"
                                                value={sensor.default}
                                                onChange={setSensorConfigValue.bind(this, index, 'default')}
                                                errors={errors?.sensors !== undefined ? errors.sensors[index]?.default : undefined}
                                            />
                                        </AccordionPanel>
                                    </AccordionItem>)}
                                </Accordion>

                                <Button
                                    colorScheme={"green"}
                                    onClick={addSensor}
                                    mx="auto"
                                    display="block"
                                    mt={5}
                                >Add sensor</Button>
                            </TabPanel>

                            <TabPanel p={0}>
                                <Accordion allowMultiple>
                                    {configs.map((config, index) => <AccordionItem key={index}>
                                        <AccordionButton>
                                            <Box as="span" flex='1' textAlign='left'
                                                 color={errors?.configs !== undefined && configs.sensors[index] !== undefined ? "red" : null}>
                                                Sensor #{index}: {config.title}
                                            </Box>
                                            <AccordionIcon/>
                                        </AccordionButton>

                                        <AccordionPanel px={0}>
                                            <Button
                                                colorScheme={"red"}
                                                ml={2}
                                                aria-label={"Delete"}
                                                isDisabled={config.length === 1}
                                                onClick={deleteConfig.bind(this, index)}
                                                display={"block"}
                                                mx={"auto"}
                                            >
                                                Delete <DeleteIcon ml={2}/>
                                            </Button>

                                            <InputGroup
                                                id={`config_${index}_title`}
                                                label={`Config title:`}
                                                type="text"
                                                value={config.title}
                                                onChange={setConfigValue.bind(this, index, 'title')}
                                                errors={errors?.configs !== undefined ? errors.configs[index]?.title : undefined}
                                            />
                                            <InputGroup
                                                id={`config_${index}_query`}
                                                label={`Config Query JSON Path:`}
                                                type="text"
                                                value={config.query}
                                                onChange={setConfigValue.bind(this, index, 'query')}
                                                errors={errors?.configs !== undefined ? errors.configs[index]?.query : undefined}
                                            />
                                        </AccordionPanel>
                                    </AccordionItem>)}
                                </Accordion>

                                <Button
                                    colorScheme={"green"}
                                    onClick={addConfig}
                                    mx="auto"
                                    display="block"
                                    mt={5}
                                >Add configuration</Button>
                            </TabPanel>
                        </TabPanels>
                    </Tabs>

                    <Box display={"flex"} justifyContent={"space-between"} mt={5}>
                        <Button
                            colorScheme={"yellow"}
                            onClick={cancel}
                        >Cancel</Button>

                        <Button
                            colorScheme={"green"}
                            onClick={submit}
                        >Create</Button>
                    </Box>
                </CardBody>
            </Card>
        </Container>
    </Center>;

}